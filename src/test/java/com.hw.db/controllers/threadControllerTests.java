package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.models.User;
import com.hw.db.models.Thread;
import com.hw.db.models.Post;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class threadControllerTests {
    private User user = new User(
            "Vanya",
            "user@foo.bar",
            "Ivan Ivanov",
            "about me"
    );
    private final Timestamp t = Timestamp.from(Instant.now());
    private Post post = new Post(
            "Vanya",
            t,
            "forum",
            "message",
            0,
            0,
            true
    );
    private Vote vote = new Vote(
            "Vanya",
            1
    );
    private Thread thread;
    private threadController controller;
    private List<User> users;
    private List<Post> posts;

    @BeforeEach
    @DisplayName("init")
    void init() {
        thread = new Thread(
                "Vanya",
                t,
                "forum",
                "message",
                "1",
                "title",
                1
        );
        thread.setId(1);
        users = List.of(user);
        posts = List.of(post);
        controller = new threadController();
    }

    @Test
    @DisplayName("Post Creation")
    public void CreatePostTest() {
        MockedStatic<ThreadDAO> threadDao = Mockito.mockStatic(ThreadDAO.class);
        threadDao.when(() -> ThreadDAO.getThreadById(1)).thenReturn(thread);
        threadDao.when(() -> ThreadDAO.getPosts(1, 1, 2, "sort", true)).thenReturn(posts);
        ResponseEntity responseEntity = controller.Posts("1", 1, 2, "sort", true);
        assertEquals(ResponseEntity.status(HttpStatus.OK).body(posts), responseEntity);
    }
}